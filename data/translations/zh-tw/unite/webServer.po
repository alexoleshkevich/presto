# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-09-02 10:22-02:00\n"
"PO-Revision-Date: 2009-10-26 00:15+0800\n"
"Last-Translator: Jack Wu <sjackwu@yahoo.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Error page title text when a resource is not found
#: templates/fileSharing.html
msgid "Folder or file not found"
msgstr "找不到檔案或資料夾"

#. A table header that describes the access level for a file in the selected folder.
#: templates/fileSharing.html
msgid "Access"
msgstr "權限"

#. A table header that describes the name of a file in the selected folder.
#: templates/fileSharing.html
msgid "Name"
msgstr "名稱"

#. A table header that describes the size of a file in the selected folder.
#: templates/fileSharing.html
msgid "Size"
msgstr "大小"

#. A table header that describes the time a file last got modified in the selected folder.
#: templates/fileSharing.html
msgid "Time"
msgstr "時間"

#. A link for a visitor to download a file from the owner's selected folder.
#: templates/fileSharing.html
msgid "Download"
msgstr "下載"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 folder"
msgstr "1 個資料夾"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} folders"
msgstr "{counter} 個資料夾"

#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "and"
msgstr "和"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 file"
msgstr "1 個檔案"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} files"
msgstr "{counter} 個檔案"

#. Text displayed when there is no index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid ""
"Visitors will see the files in this folder, as there is no index.html file "
"to display."
msgstr "訪客將會看到資料夾裡的檔案，因為裡面沒有 index.html 可供顯示。"

#. Text displayed when there is no index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid ""
"If you want visitors to see an index page, create an index.html file or <A "
"href=\"?create_index=true\">generate a sample file</A>."
msgstr ""
"如果你想要讓訪客能看到一個首頁，而不是檔案列表，請建立一個 index.html 檔案或 "
"<A href=\"?create_index=true\">產生一個範例檔案</A>."

#. Text displayed when there is an index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid ""
"<EM>This folder contains an index.html file.</EM> This is the first page "
"visitors to your Web Server will see: <A href=\"{index}\">{index}</A>"
msgstr ""
"<EM>這個資料夾裡有個 index.html 檔案。</EM> 這個檔案是你 Web Server 的訪客會"
"首先看到的頁面: <A href=\"{index}\">{index}</A>"

#. Message shown when the original share folder selected by the owner can't be accessed
#. Properties... text comes from the right-click menu of the application in the Unite panel.
#: templates/noSharedMountpoint.html
msgid ""
"Folder not found. To select a new one, right-click <STRONG>{serviceName}</"
"STRONG> in the Unite panel, and choose <STRONG>Properties</STRONG>"
msgstr ""
"找不到資料夾，要選擇一個新資料夾，請在 Unite 的面板裡 <STRONG>{serviceName}</"
"STRONG> 按右鍵，並選 <STRONG>內容</STRONG>"

#. Text in the generated index.html file.
#: templates/index.html
msgid ""
"This sample Web page <STRONG>index.html</STRONG> was created when you "
"clicked \"generate a sample file\" in a folder without an index.html file. "
"Edit it to suit your taste. This is the first page visitors to your Web "
"Server will see."
msgstr ""
"這個範例頁面 <STRONG>index.html</STRONG> 是你在缺少 index.html 的資料夾裡按"
"了 \"產生一個範例檔案\" 所建立的。你可以編輯這個檔案以符合你需求，這個檔案是"
"你 Web Server 的訪客會看到的第一個頁面。"

#. A header in the generated index.html that describes a section of the page
#. for the viewer to get resources to learn Web development.
#: templates/index.html
msgid "Resources"
msgstr "資源"

#. Text in the generated index.html file. Followed by a link to the Opera Web Standards Curriculum.
#: templates/index.html
msgid "To learn more about Web development and design, see the"
msgstr "更多關於網頁開發與設計的資訊，請參考"
